export const getCartItemsNumber = () => {
  if (localStorage.length > 0 && localStorage.key(0) !== 'price') {
    /* We store the keys of the items in localstorage in an array */
    const keys = Object.keys(localStorage)
    let totalQty = 0
    for (let i = 0; i < keys.length; i++) {
      /* We access the items with the keys previously stored */
      const item = JSON.parse(localStorage.getItem(keys[i]))
      /* We get the items quantity and add it to a total */
      const qty = parseInt(item.qty)
      totalQty += qty
    }
    return totalQty
  } else {
    /* If there isn't any item in localStorage, we return 0 */
    return 0
  }
}

export const showCartItemsNumber = (num) => {
/* The number passed as param is shown in the DOM  (header) */
  const itemsNumberSpan = document.getElementById('badge')
  itemsNumberSpan.textContent = num
}

export const insertDecimal = (num) => {
/* Divides the number passed as param by 100 and fixes to 2 the digits after the decimal point */
  return (num / 100).toFixed(2)
}

/* Transforms number passed as params into a formatted price in euros */
export const transformToCurrency = (num) => {
  const priceInEur = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(num)
  return priceInEur
}
