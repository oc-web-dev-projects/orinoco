import { indexExports } from './index.js'
import { itemExports } from './item.js'
import { cartExports } from './cart.js'
import { orderExports } from './order.js'
import { utilsExports } from './utils.js'
const Index = indexExports.Index
const Item = itemExports.Item
const Cart = cartExports.Cart
const Order = orderExports.Order
const parseRequestUrl = utilsExports.parseRequestUrl
const emptyMainInnerHTML = utilsExports.emptyMainInnerHTML
const updateCartNumber = utilsExports.updateCartNumber

/* Associates each page object with a url file path */
const routes = {
  '/': Index,
  '/item/:id': Item,
  '/cart': Cart,
  '/order': Order
}

/* Transforms the request returned by parseRequestUrl(hash) to suit the routes listed above */
const urlParser = () => {
  const hash = location.hash
  const request = parseRequestUrl(hash)
  const parsedURL = (request.resource ? '/' + request.resource : '/') + (request.id ? '/:id' : '')
  return parsedURL
}
/* Returns the routes object value associated with the request resource, logs an error in absence */
const checkSupportedRoutes = () => {
  const parsedURL = appExports.urlParser()
  if (routes[parsedURL]) {
    return routes[parsedURL]
  } else {
    console.error('Error404', routes[parsedURL])
  }
}

const router = async () => {
  const page = appExports.checkSupportedRoutes()
  emptyMainInnerHTML()
  await page.render()
  await page.after_render()
}

/* Calls the router and updates items'number in header whenever the url hash changes */
const hashChangeListener = () => {
  window.addEventListener('hashchange', async () => { appExports.router() })
  window.addEventListener('hashchange', updateCartNumber)
}
/* Calls the router and updates items'number in header whenever the page is loaded */
const loadListener = () => {
  window.addEventListener('load', async () => { appExports.router() })
  window.addEventListener('load', updateCartNumber)
}

const appExports = { router, urlParser, checkSupportedRoutes, hashChangeListener, loadListener }

export { appExports }
