import { getCartItemsNumber, showCartItemsNumber, insertDecimal, transformToCurrency } from './global.js'
import { utilsExports } from './utils.js'
const parseRequestUrl = utilsExports.parseRequestUrl

const addToLocalStorage = (key, value) => {
  /* If the item is already in storage it adds 1 to the item quantity */
  if (localStorage.getItem(key) != null) {
    itemExports.addQuantity(key)
  } else {
    localStorage.setItem(key, value)
  }
  /* Updates number of cart items displayed in header */
  const itemsNumber = getCartItemsNumber()
  showCartItemsNumber(itemsNumber)
}
/* Adds 1 to item quantity in localStorage */
const addQuantity = (key) => {
  const item = JSON.parse(localStorage.getItem(key))
  const itemQty = item.qty
  item.qty = itemQty + 1
  localStorage.setItem(key, JSON.stringify(item))
}
/* Calls getCustomTeddy function on click */
const addToCart = () => {
  const btn = document.getElementById('add-to-cart')
  btn.addEventListener('click', itemExports.getCustomTeddy)
}
const getCustomTeddy = () => {
  /* Stores the item info in an object */
  const menu = document.getElementById('teddy-custom')
  const selectedOption = menu.value
  const btn = document.getElementById('add-to-cart')
  const teddy = {
    id: btn.getAttribute('data-id'),
    name: btn.getAttribute('data-name'),
    price: btn.getAttribute('data-price'),
    image: btn.getAttribute('data-image'),
    color: selectedOption,
    qty: 1
  }
  const teddyValue = JSON.stringify(teddy)
  /* Creates an identifier key with the name and the color of the item (e.g.: Arnold_Brown) */
  const teddyKey = btn.getAttribute('data-name').replace(/ /g, '_') + '_' + selectedOption
  /* Stores the item in local storage with the new identifier key */
  itemExports.addToLocalStorage(teddyKey, teddyValue)
}

/* Sets the structure to append the items and the class to apply style */
const createItemDOM = () => {
  var main = document.querySelector('main')
  main.setAttribute('class', 'item')
  const itemDOM = `<div class="item-card">
  <div class="item-picture">
  <img src="" alt="ours en peluche" id="teddy-picture">
  </div>
  <div class="item-info">
  <h1 id="teddy-name"></h1>
  <p id="teddy-description"></p>
  <p id="teddy-price"></p>
  <form>
  <label for="teddy-custom">Choisissez une couleur :</label>
  <select name="customize" id="teddy-custom">
  </select>
  </form>
  <button class="btn" id="add-to-cart" data-id="" data-name="" data-price="" data-image="">Ajouter au panier</button>
  </div>
  </div>`
  main.innerHTML = itemDOM
}
const getTeddyRequest = (id) => {
  return new Promise((resolve, reject) => {
    try {
      if (!id || id === '') {
        throw new Error('missing or empty ID')
      }
      const request = new XMLHttpRequest()
      request.onload = () => {
        if (request.status >= 200 && request.status < 300) {
          resolve({
            data: request.response,
            status: request.status
          })
        } else if (request.status >= 300 && request.status < 500) {
          resolve({
            message: request.statusText,
            status: request.status
          })
        } else {
          reject(request.statusText)
        }
      }
      request.onerror = () => reject(request.statusText)
      request.open('GET', `http://localhost:3002/api/teddies/${id}`)
      request.send()
    } catch (error) {
      reject(error)
    }
  })
}
/* Displays the item received by the request to the API */
const appendTeddyDOM = (data) => {
  return new Promise((resolve, reject) => {
    if (data.status < 300) {
      try {
        const teddy = JSON.parse(data.data)
        document.getElementById('teddy-picture').setAttribute('src', teddy.imageUrl)
        document.getElementById('teddy-name').innerText = teddy.name
        document.getElementById('teddy-description').innerText = teddy.description
        let teddyPrice = teddy.price
        teddyPrice = insertDecimal(teddyPrice)
        teddyPrice = transformToCurrency(teddyPrice)
        document.getElementById('teddy-price').innerText = teddyPrice
        document.getElementById('add-to-cart').setAttribute('data-id', teddy._id)
        document.getElementById('add-to-cart').setAttribute('data-name', teddy.name)
        document.getElementById('add-to-cart').setAttribute('data-description', teddy.description)
        document.getElementById('add-to-cart').setAttribute('data-price', teddy.price)
        document.getElementById('add-to-cart').setAttribute('data-image', teddy.imageUrl)
        const menu = document.getElementById('teddy-custom')
        for (const color of teddy.colors) {
          const option = document.createElement('option')
          menu.appendChild(option)
          option.setAttribute('value', color)
          option.innerText = color
        }
        resolve(data)
      } catch (error) {
        reject(error)
      }
    } else {
      reject(data.status)
    }
  })
}

/* All the functions are stored in this object so the router can access them to load the page */
const Item = {
  render: () => {
    itemExports.createItemDOM()
  },
  after_render: async () => {
    try {
      const url = parseRequestUrl(location.hash)
      const teddy = await itemExports.getTeddyRequest(url.id)
      await itemExports.appendTeddyDOM(teddy)
      itemExports.addToCart()
    } catch (error) {
      console.error('error', error)
    }
  }
}

const itemExports = {
  addQuantity,
  addToLocalStorage,
  addToCart,
  getCustomTeddy,
  createItemDOM,
  getTeddyRequest,
  appendTeddyDOM,
  Item
}

export { itemExports }
