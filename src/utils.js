import { getCartItemsNumber, showCartItemsNumber } from './global.js'

const parseRequestUrl = (hash) => {
  /* Stores an array of each part of the URL after the hash, splitted by the slash */
  /* e.g.: http://localhost:3001/#/item/id >>> /item/id >>> ['', 'item', 'id'] */
  const split = hash.slice(1).split('/')
  return {
    resource: split[1] || null,
    id: split[2] || null
  }
}
/* Updates the number of cart items showed in header */
const updateCartNumber = () => {
  const cartItemsNumber = getCartItemsNumber()
  showCartItemsNumber(cartItemsNumber)
}

/* Empties main element of its html content (called before rendering a new page) */
const emptyMainInnerHTML = () => {
  const main = document.querySelector('main')
  main.innerHTML = ''
}

const utilsExports = { parseRequestUrl, updateCartNumber, emptyMainInnerHTML }

export { utilsExports }
