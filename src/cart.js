import { insertDecimal, transformToCurrency, showCartItemsNumber, getCartItemsNumber } from './global.js'

const getStoredItems = () => {
  const cartItems = []
  /* Stores the keys of the local storage elements */
  const keys = Object.keys(localStorage)
  for (let i = 0; i < keys.length; i++) {
  /* for each element in the storage it creates an object with the key and the value */
    const obj = {
      storageKey: keys[i],
      storageItem: JSON.parse(localStorage.getItem(keys[i]))
    }
    /* Stores each object in the cartItems array */
    cartItems.push(obj)
  };
  return cartItems
}

const checkIfCartIsEmpty = () => {
  if (localStorage.length <= 0) {
    return true
  } else {
    return false
  }
}

const cartIsEmpty = () => {
  /* Shows the 'empty cart' notice and the button link to index */
  const emptyCartP = document.getElementById('empty-cart')
  emptyCartP.style.display = 'block'

  const browseItems = document.getElementById('browse-items')
  browseItems.style.display = 'block'
  /* Hides the order button, the cart total and the form */
  const toggleBtn = document.getElementsByClassName('toggle-div')[0]
  toggleBtn.style.display = 'none'

  const cartTotal = document.getElementById('cart-total')
  cartTotal.style.display = 'none'

  const form = document.getElementById('order-form')
  form.style.display = 'none'
}

const showCartItems = (cartItemsArray) => {
  /* If the cart is empty it calls the cartIsEmpty function */
  if (cartExports.checkIfCartIsEmpty()) {
    cartExports.cartIsEmpty()
  } else {
  /* Creates the DOM elements and display the cart items passed as params */
    const cartSection = document.getElementById('cart-items-list')
    for (let i = 0; i < cartItemsArray.length; i++) {
      const cartItem = document.createElement('div')
      cartItem.setAttribute('class', 'cart-item')
      cartItem.setAttribute('id', cartItemsArray[i].storageKey)
      cartSection.appendChild(cartItem)

      const image = document.createElement('img')
      image.src = cartItemsArray[i].storageItem.image
      image.setAttribute('class', 'image')
      cartItem.appendChild(image)

      const name = document.createElement('h2')
      name.textContent = cartItemsArray[i].storageItem.name
      name.setAttribute('class', 'name')
      cartItem.appendChild(name)

      const color = document.createElement('p')
      color.textContent = cartItemsArray[i].storageItem.color
      color.setAttribute('class', 'color')
      cartItem.appendChild(color)

      const controls = document.createElement('div')
      controls.setAttribute('class', 'controls')
      cartItem.appendChild(controls)

      const minus = document.createElement('span')
      minus.textContent = '-'
      minus.setAttribute('data-id', cartItemsArray[i].storageKey)
      minus.setAttribute('class', 'minus')
      controls.appendChild(minus)
      minus.addEventListener('click', decrementCart)

      const quantity = document.createElement('span')
      quantity.textContent = cartItemsArray[i].storageItem.qty
      quantity.className = 'quantity'
      controls.appendChild(quantity)

      const plus = document.createElement('span')
      plus.textContent = '+'
      plus.setAttribute('data-id', cartItemsArray[i].storageKey)
      plus.setAttribute('class', 'plus')
      controls.appendChild(plus)
      plus.addEventListener('click', incrementCart)

      const price = document.createElement('p')
      let itemPrice = cartItemsArray[i].storageItem.price * cartItemsArray[i].storageItem.qty
      itemPrice = insertDecimal(itemPrice)
      const cost = transformToCurrency(itemPrice)
      price.textContent = cost
      price.className = 'price'
      cartItem.appendChild(price)

      const trashBtn = document.createElement('button')
      trashBtn.setAttribute('class', 'remove-btn')
      trashBtn.setAttribute('id', cartItemsArray[i].storageKey)
      trashBtn.textContent = 'x'
      cartItem.appendChild(trashBtn)
    }
  }
}
const calculateTotalPrice = (cartItems) => {
  /* We pass the array of stored items as param */
  let total = 0
  for (let i = 0; i < cartItems.length; i++) {
    for (let i = 0; i < cartItems.length; i++) {
      /* Transforms the price string into an integer */
      const itemPrice = parseInt(cartItems[i].storageItem.price)
      /* The price is transformed as follows: 3500 into 35,00 */
      const decimalItemPrice = insertDecimal(itemPrice)
      /* MultiplIes the item price by the quantity */
      const itemPriceByQty = decimalItemPrice * cartItems[i].storageItem.qty
      total += itemPriceByQty
    }
    return total
  }
}

const showTotalPrice = (num) => {
  const totalPrice = transformToCurrency(num)
  const totalPriceSpan = document.getElementById('total')
  totalPriceSpan.textContent = totalPrice
}

const removeItemFromCart = () => {
  const removeButtons = document.getElementsByClassName('remove-btn')
  for (let i = 0; i < removeButtons.length; i++) {
    /* Adds a click listener to the remove buttons of each item in the cart */
    removeButtons[i].addEventListener('click', function (event) {
      event.preventDefault()
      const item = event.target
      const itemId = item.id
      /* Removes the item rown in the cart thanks to the item id retrieved on the click */
      cartExports.removeItemRow(itemId)
      /* Calls the necessary functions to update the cart in consequence */
      showCartItemsNumber(getCartItemsNumber())
      if (cartExports.checkIfCartIsEmpty()) {
        cartExports.cartIsEmpty()
      } else {
        cartExports.updateTotalPrice()
      }
    })
  }
}
const removeItemRow = (itemId) => {
  /* Removes item from localStorage and the item row in the cart section */
  const row = document.getElementById(itemId)
  localStorage.removeItem(itemId)
  row.remove()
}
const subtractPrice = (price) => {
  /* Subtracts the price passed as params to the cart total price */
  const total = document.getElementById('total')
  let totalPrice = total.textContent
  totalPrice = parseInt(totalPrice)
  totalPrice -= price
  return totalPrice
}
const addPrice = (price) => {
  /* Adds the price passed as params to the cart total price */
  const total = document.getElementById('total')
  let totalPrice = total.textContent
  totalPrice = parseInt(totalPrice)
  totalPrice += price
  return totalPrice
}
const decrementCart = (event) => {
  /* Called when minus is clicked */
  const itemId = event.target.getAttribute('data-id')
  /* Substract 1 to item quantity in localStorage */
  cartExports.subtractQuantity(itemId)
  /* If item quantity in localStorage is 0, it removes the item row from the cart */
  cartExports.checkItemQty(itemId)
  /* Updates cart info in consequence */
  showCartItemsNumber(getCartItemsNumber())
  if (cartExports.checkIfCartIsEmpty()) {
    cartExports.cartIsEmpty()
  } else {
    cartExports.updateQuantity(itemId)
    cartExports.updatePrice(itemId)
    cartExports.updateTotalPrice()
  }
}
const incrementCart = (event) => {
  /* Called when plus is clicked */
  const itemId = event.target.getAttribute('data-id')
  /* Adds 1 to item quantity in localStorage */
  cartExports.addQuantity(itemId)
  /* Updates cart info in consequence */
  cartExports.updateQuantity(itemId)
  cartExports.updatePrice(itemId)
  cartExports.updateTotalPrice()
  showCartItemsNumber(getCartItemsNumber())
}

const updateTotalPrice = () => {
  const totalSpan = document.getElementById('total')
  const prices = document.getElementsByClassName('price')
  let total = 0
  /* Recalculates total price based on the prices displayed in cart */
  for (let i = 0; i < prices.length; i++) {
    let price = prices[i].textContent.replace('€', '')
    price = parseInt(price)
    total += price
  }
  const newTotal = transformToCurrency(total)
  totalSpan.textContent = newTotal
}
const checkItemQty = (key) => {
  /* If item quantity equals 0 it removes the item row from the cart */
  const item = JSON.parse(localStorage.getItem(key))
  if (item.qty <= 0) {
    cartExports.removeItemRow(key)
  }
}
const addQuantity = (key) => {
  /* Adds 1 to item quantity and updates it in localStorage */
  const item = JSON.parse(localStorage.getItem(key))
  const itemQty = item.qty
  item.qty = itemQty + 1
  localStorage.setItem(key, JSON.stringify(item))
}
const subtractQuantity = (key) => {
  /* Substracts 1 to item quantity and updates it in localStorage */
  const item = JSON.parse(localStorage.getItem(key))
  const itemQty = item.qty
  item.qty = itemQty - 1
  localStorage.setItem(key, JSON.stringify(item))
}
const updatePrice = (itemId) => {
  /* Updates the item price showed in the cart based on the selected number of items */
  const row = document.getElementById(itemId)
  if (row) {
    const priceDiv = row.getElementsByClassName('price')[0]
    const item = JSON.parse(localStorage.getItem(itemId))
    let itemPrice = item.price * item.qty
    itemPrice = transformToCurrency(insertDecimal(itemPrice))
    priceDiv.innerText = itemPrice
  }
}
const updateQuantity = (itemId) => {
  /* Updates the item quantity showed in the cart */
  const row = document.getElementById(itemId)
  if (row) {
    const itemQuantitySpan = row.getElementsByClassName('quantity')[0]
    const item = JSON.parse(localStorage.getItem(itemId))
    const itemQty = item.qty
    itemQuantitySpan.innerText = itemQty
  }
}
const itemsIdToOrderArray = () => {
  /* Stores the items to order id in an array */
  const items = []
  const keys = Object.keys(localStorage)
  for (let i = 0; i < keys.length; i++) {
    const itemToParse = localStorage.getItem(keys[i])
    const itemObject = JSON.parse(itemToParse)
    items.push(itemObject.id)
  }
  /* Creates a set of unique ids based on the array of items ids */
  const uniqueSet = new Set(items)
  /* Puts the unique ids in a new array */
  const productId = [...uniqueSet]
  return productId
}
const toggleContactForm = () => {
  const validateBtn = document.getElementById('toggle-button')
  const contactForm = document.getElementById('order-form')
  validateBtn.addEventListener('click', function () {
    if (contactForm.style.display === 'none') {
      /* Shows the form and scrolls to it automatically */
      contactForm.style.display = 'block'
      cartExports.scrollToForm(contactForm)
    } else {
      /* Hides the form */
      contactForm.style.display = 'none'
    }
  })
}
const scrollToForm = (element) => {
  /* top is the number of pixels from the top of the closest relatively positioned parent element */
  const top = element.offsetTop
  window.scrollTo(0, top)
}
const customerContactObject = () => {
  /* Creates and object with the contact info filled by the user */
  const lastName = document.getElementById('lastName').value
  const firstName = document.getElementById('firstName').value
  const address = document.getElementById('address').value
  const city = document.getElementById('city').value
  const email = document.getElementById('email').value
  const contact = {
    lastName: lastName,
    firstName: firstName,
    address: address,
    city: city,
    email: email
  }
  return contact
}
const storeTotalPrice = () => {
  /* Stores the total price of the order */
  const price = document.getElementById('total').innerHTML
  localStorage.setItem('price', price)
}
const openOrderWindow = () => {
  /* The new page must be opened in the actual window */
  window.open('/#/order', '_self')
}
const orderPost = () => {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest()
    const contact = cartExports.customerContactObject()
    const products = cartExports.itemsIdToOrderArray()
    /* Stores the items ordered and the customer information contact in the body object */
    const body = {
      contact: contact,
      products: products
    }
    const price = document.getElementById('total').textContent
    request.onload = () => {
      if (request.status >= 200 && request.status < 300) {
        /* The promise resolves with the API response, the status and the order's total price  */
        resolve({
          data: request.responseText,
          status: request.status,
          price: price
        })
      } else if (request.status >= 300 && request.status < 500) {
        resolve({
          message: request.statusText,
          status: request.status
        })
      } else {
        /* Status over 500 means error, promise is rejected  */
        reject(request.statusText)
      }
    }
    request.onerror = () => reject(request.statusText)
    request.open('POST', 'http://localhost:3002/api/teddies/order')
    request.setRequestHeader('Content-Type', 'application/json')
    /* Sends to the API the order and the customer contact info previsously stored in the body object */
    request.send(JSON.stringify(body))
  })
}

const storeResponse = (data) => {
  return new Promise((resolve, reject) => {
    try {
      if (!data || data === '') {
        throw new Error('missing or empty ID')
      }
      if (data.status < 300) {
        /* Empties localStorage from cart items and stores the order info */
        localStorage.clear()
        localStorage.setItem('order', data.data)
        localStorage.setItem('price', data.price)
        resolve(data)
      } else {
        reject(new Error(`${data}`))
      }
    } catch (error) {
      reject(error)
    }
  })
}
const sendOrder = async (event) => {
  event.preventDefault()
  try {
    const orderPost = await cartExports.orderPost()
    /* Stores the important data and open the order page in the actual window */
    await cartExports.storeResponse(orderPost)
    cartExports.openOrderWindow()
  } catch (error) {
    console.error(error)
  }
}
const sendOrderOnClick = () => {
  /* When the button to order is clicked, it sends the post request to the API */
  document.getElementById('form').addEventListener('submit', cartExports.sendOrder)
}

/* Sets the structure to append the items and the class to apply style */
const createCartDOM = () => {
  const main = document.querySelector('main')
  main.setAttribute('class', 'cart')
  const content =
  '<div class="heading"><h1>Mon panier</h1></div><div id="cart-items-list"></div><div id="cart-total"><p id="total-amount">Montant total : <span id="total"></span></p></div>' +
  '<div class="toggle-div">' +
  '<button class="toggle-button" id="toggle-button">' +
  'Valider mon panier' +
  '</button>' +
  '</div>' +
  '<p id ="empty-cart" class="empty-cart" style="display: none">Votre panier est vide</p>' +
  '<a href="/#/" class="browse-items" id="browse-items" style="display: none">Continuer vos achats</a>' +
  '<div class="order-form" id="order-form" style="display: none">' +
  '<form id="form">' +
  '<h3>Informations de commande</h3>' +
  '<label for="firstName">Nom :</label>' +
  '<input type="text" id="lastName" name="LastName" pattern="^[a-zA-Z\u00C0-\u00FF]+(([\',. -][a-zA-Z \u00C0-\u00FF])?[a-zA-Z\u00C0-\u00FF ]*)*$"required>' +
  '<label for="firstName">Prénom :</label>' +
  '<input type="text" id="firstName" name="firstName" pattern="^[a-zA-Z\u00C0-\u00FF]+(([\',. -][a-zA-Z \u00C0-\u00FF])?[a-zA-Z\u00C0-\u00FF ]*)*$"required>' +
  '<label for="address">Adresse :</label>' +
  '<input type="text" id="address" name="address" minlength="4" pattern="^([0-9a-zA-Z \u00C0-\u00FF])+[a-zA-Z\u00C0-\u00FF]+(([\',. -][a-zA-Z \u00C0-\u00FF])+[a-zA-Z\u00C0-\u00FF ]*)*$" required>' +
  '<label for="city">Ville de résidence :</label>' +
  '<input type="text" id="city" name="city" pattern="^[a-zA-Z\u00C0-\u00FF]+(([\',. -][a-zA-Z \u00C0-\u00FF])?[a-zA-Z\u00C0-\u00FF ]*)*$"required>' +
  '<label for="email">Email :</label>' +
  ' <input type="email" id="email" name="email" required>' +
  '<input type="submit" value="Commander" id="send-btn">' +
  '</form>' +
  '</div>'
  main.innerHTML = content
}

/* All the functions are stored in this object so the router can access them to load the page */
const Cart = {
  render: () => {
    cartExports.createCartDOM()
  },
  after_render: () => {
    const storedItems = cartExports.getStoredItems()
    cartExports.showCartItems(storedItems)
    const totalPrice = cartExports.calculateTotalPrice(storedItems)
    cartExports.showTotalPrice(totalPrice)
    cartExports.removeItemFromCart()
    cartExports.toggleContactForm()
    cartExports.sendOrderOnClick()
  }
}

const cartExports = {
  getStoredItems,
  checkIfCartIsEmpty,
  cartIsEmpty,
  showCartItems,
  decrementCart,
  incrementCart,
  calculateTotalPrice,
  showTotalPrice,
  removeItemFromCart,
  removeItemRow,
  updateTotalPrice,
  subtractPrice,
  addPrice,
  checkItemQty,
  subtractQuantity,
  addQuantity,
  updatePrice,
  updateQuantity,
  itemsIdToOrderArray,
  toggleContactForm,
  scrollToForm,
  customerContactObject,
  storeTotalPrice,
  openOrderWindow,
  orderPost,
  storeResponse,
  sendOrder,
  sendOrderOnClick,
  createCartDOM,
  Cart
}

export { cartExports }
