const getOrderFromStorage = () => {
  let order = localStorage.getItem('order')
  order = JSON.parse(order)
  return order
}

const getPriceFromStorage = () => {
  const price = localStorage.getItem('price')
  return price
}
/* Sets the structure to append the order info and sets the class to apply style */
const createOrderDOM = () => {
  const main = document.querySelector('main')
  main.setAttribute('class', 'order')
  const content = `<div class="order-info">
  <h1>Bonjour <span id="customer-name"></span></h1>
  <p>Merci de votre achat sur Orinoco. Nous avons bien reçu votre commande. Ci-dessous, vous trouverez les informations concernant votre achat.</p>
  <p>
  <strong>Nº de commande :</strong> <br><span id="order-id"></span>
  </p>
  <p>
  <strong>Prix total :</strong> <br><span id='order-price'></span>
  </p>
  <p>
  <strong>Informations de contact :</strong>
  </p>
  <p class="indent"><strong>Nom : </strong><span id='customer-last-name'></span></p>
  <p class="indent"><strong>Prénom : </strong><span id='customer-first-name'></span></p>
  <p class="indent"><strong>Adresse : </strong> <span id='customer-address'></span></p>
  <p class="indent"> <strong>Ville : </strong><span id='customer-city'></span></p>
  <p class="indent"><strong>Email: </strong><span id='customer-email'></span></p>
  </div>
  <div class="continue-shopping">
  <a href="/#/" class="browse-items">Continuer vos achats</a>
  </div>`
  main.innerHTML = content
}

/* Displays the order information */
const showOrderInfo = () => {
  const price = orderExports.getPriceFromStorage()
  const order = orderExports.getOrderFromStorage()
  const customer = order.contact

  document.getElementById('order-id').textContent = order.orderId
  document.getElementById('order-price').textContent = price
  document.getElementById('customer-last-name').textContent = customer.lastName
  document.getElementById('customer-first-name').textContent = customer.firstName
  document.getElementById('customer-address').textContent = customer.address
  document.getElementById('customer-city').textContent = customer.city
  document.getElementById('customer-email').textContent = customer.email
  document.getElementById('customer-name').textContent = customer.firstName
}

const clearLocalStorage = () => {
  localStorage.clear()
}

/* All the functions are stored in this object so the router can access them to load the page */
const Order = {
  render: () => {
    orderExports.createOrderDOM()
  },
  after_render: () => {
    orderExports.showOrderInfo()
    orderExports.clearLocalStorage()
  }
}

const orderExports = { getOrderFromStorage, getPriceFromStorage, createOrderDOM, showOrderInfo, clearLocalStorage, Order }

export { orderExports }
