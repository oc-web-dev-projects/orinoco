import { insertDecimal, transformToCurrency } from './global.js'

const getTeddiesRequest = () => {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest()
    request.onload = () => {
      if (request.status >= 200 && request.status < 300) {
        resolve({
          data: request.responseText,
          status: request.status
        })
      } else if (request.status >= 300 && request.status < 500) {
        resolve({
          message: request.responseText,
          status: request.status
        })
      } else {
        reject(request.statusText)
      }
    }
    request.open('GET', 'http://localhost:3002/api/teddies')
    request.send()
  })
}
/* Sets the structure to append the items and the class to apply style */
const createIndexDOM = () => {
  const main = document.querySelector('main')
  main.setAttribute('class', 'index')
  const title = document.createElement('h1')
  title.textContent = 'Ours en peluche faits à la main'
  const container = document.createElement('div')
  container.id = 'cards-container'
  container.setAttribute('class', 'cards-container')
  main.appendChild(title)
  main.appendChild(container)
}
const appendTeddiesDOM = (data) => {
  /* Creates the elements to display the items received by the request to the API */
  const teddies = JSON.parse(data.data)
  for (let i = 0; i < teddies.length; i++) {
    var container = document.getElementById('cards-container')
    var article = document.createElement('article')
    var img = document.createElement('img')
    var info = document.createElement('div')
    var titleName = document.createElement('h2')
    var description = document.createElement('p')
    var priceDiv = document.createElement('div')
    var price = document.createElement('p')
    var button = document.createElement('a')
    var footer = document.createElement('div')
    priceDiv.setAttribute('class', 'article-price')
    footer.setAttribute('class', 'article-footer')
    button.setAttribute('class', 'btn')
    button.setAttribute('href', '/#/item/' + teddies[i]._id)
    button.textContent = 'Voir produit'
    article.setAttribute('id', teddies[i]._id)
    img.setAttribute('class', 'picture')
    img.setAttribute('src', teddies[i].imageUrl)
    info.setAttribute('class', 'info')
    titleName.textContent = teddies[i].name
    description.textContent = teddies[i].description
    let teddyPrice = teddies[i].price
    teddyPrice = insertDecimal(teddyPrice)
    teddyPrice = transformToCurrency(teddyPrice)
    price.textContent = teddyPrice
    container.appendChild(article)
    article.appendChild(img)
    article.appendChild(info)
    article.appendChild(priceDiv)
    article.appendChild(footer)
    info.appendChild(titleName)
    info.appendChild(description)
    priceDiv.appendChild(price)
    footer.appendChild(button)
  }
}
/* All the functions are stored in this object so the router can access them to load the page */
const Index = {
  render: () => {
    indexExports.createIndexDOM()
  },
  after_render: async () => {
    try {
      const teddies = await indexExports.getTeddiesRequest()
      appendTeddiesDOM(teddies)
    } catch (error) {
      console.error('error', error)
    }
  }
}

const indexExports = {
  getTeddiesRequest,
  createIndexDOM,
  appendTeddiesDOM,
  Index
}

export { indexExports }
