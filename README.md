# Orinoco

5th project of the OpenClassrooms' path: Web Developer

> Build an eCommerce Website
Use the JavaScript programming language to interact with users and external services. Apply a test-driven approach to web development.

🔧 Skills acquired in this project:

- Create a test plan for an application
- Interact with a web service using JavaScript
- Manage website events with JavaScript
- Ensure data quality using data validation and cleaning

## start 

```bash
npm install
npm start
```

### TDD
 Unit testing with Jest &rArr; branch tdd

 > https://gitlab.com/oc-web-dev-projects/orinoco/-/tree/tdd


### Docker Compose 
  :whale: Run the app and the api with:
  ``` bash
  docker-compose up
  ```
